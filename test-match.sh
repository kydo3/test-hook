#!/bin/bash

MSG="updated JIRA-123"
result=$(echo $((echo $MSG | grep -Eq '(JIRA-([0-9]{3}))') && echo 'matched' || echo 'did not match'))

if [ "$result" = "did not match" ]; then
    cat "$MSG"
    echo "Your commit message must contain the Ticket ID"
    exit 1
fi