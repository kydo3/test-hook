$USER_GIT="kydo3"
$COMMITER_NAME="Kydo Solis"
$COMMITER_EMAIL="kydo@aigent.com"
$PRIVATE_TOKEN="a9kft63xxzDZG3DUU67c"
$PROJECT_ID="20657944"
$JOB_NAME="build"
$BRANCH="master"
$FILE_NAME="artifacts.zip"
$GIT_URL="https://${USER_GIT}:${PRIVATE_TOKEN}@gitlab.com/aigent/platform/delivery/aigentlauncher.git"
$SETUP_BUILDER_PATH="C:/Gitlab-Runner/builds/setup-builder"
$SETUP_BUILDER_GIT_URL="https://${USER_GIT}:${PRIVATE_TOKEN}@gitlab.com/aigent/platform/delivery/setup-builder.git"
$LAUNCHER_PATH="C:/Gitlab-Runner/builds/aigentlauncher"

function Git-Clone (){
    param($URL, $LOC)
    git clone $URL
}

function Git-Pull (){
    param($CE, $CN, $URL)
    git pull
}

function Git-Commit (){
    param($CE, $CN, $URL)
    git pull 2> $null
    git add *
    git config user.email $CE
    git config user.name $CN
    git commit --author="$CN <$CE>" -m "Build Release"
    git push "$URL" --all 2> $null
}

function Download-Artifact (){
    param($FN, $PT, $PI, $B, $JN)
    curl.exe --location --output $FN --header "PRIVATE-TOKEN: $PT" https://gitlab.com/api/v4/projects/$PI/jobs/artifacts/$B/download?job=$JN 2> $null
    Expand-Archive -Force .\$FN .\
    Remove-Item -Path .\$FN -Recurse
    cd build
    $ChildItems = Get-ChildItem -Path .\
    $BuildFileName = $ChildItems.Name
    Expand-Archive -Force .\$BuildFileName .\
    Copy-Item -Path .\app\dist\* -Destination ..\ -PassThru -Force
    Copy-Item -Path .\app\* -Destination ..\..\bin\Release -PassThru -Force
    Remove-Item -Path .\app -Recurse
    cd ../
    Remove-Item -Path .\build -Recurse
}


if (Test-Path -Path $LAUNCHER_PATH) {
    echo "Pulling aigentlauncher..."
    cd $LAUNCHER_PATH
    Git-Pull $COMMITER_EMAIL $COMMITER_NAME $GIT_URL
} else {
    echo "Cloning aigentlauncher..."
    Git-Clone $GIT_URL $LAUNCHER_PATH
    cd $LAUNCHER_PATH
}
cd dist
Download-Artifact $FILE_NAME $PRIVATE_TOKEN $PROJECT_ID $BRANCH $JOB_NAME
cd ../
Git-Commit $COMMITER_EMAIL $COMMITER_NAME $GIT_URL
cd bin
Start-Sleep -s 240
Download-Artifact $FILE_NAME $PRIVATE_TOKEN $PROJECT_ID $BRANCH $JOB_NAME
./Squirrel.exe --releasify .\Release\AigentApp.2.6.1.nupkg
cp .\Releases\Setup.msi $SETUP_BUILDER_PATH\setup
cp .\Releases\Setup.exe $SETUP_BUILDER_PATH\setup
Start-Sleep -s 20
cd $SETUP_BUILDER_PATH
Git-Commit $COMMITER_EMAIL $COMMITER_NAME $SETUP_BUILDER_GIT_URL